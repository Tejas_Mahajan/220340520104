import java.util.*;

class LinkedList{
	static Node head;
	
	static class Node{
		int data;
		Node next;
		
		Node(int x){
			this.data = x;
			this.next = null;
		}
	}
	
	void addNode(int data){
		Node new_node = new Node(data);
		if(head == null){
			head = new_node;
		}
		else{
			Node temp = head;
			while(temp.next!=null){
				temp = temp.next;
			}
			temp.next = new_node;
		}
	}
	
	Node reverse(Node node){
		Node prev = null;
		Node current = node;
		Node next = null;
		while(current!=null){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		node = prev;
		return node;
	}
	
	void displayList(Node node){
		while(node!=null){
			System.out.print(node.data+"");
			node = node.next;
		}
		System.out.println();
	}
	
	public static void main(String[] args){
		LinkedList l1 = new LinkedList();
		Scanner sc = new Scanner(System.in);
		int test = sc.nextInt();
		for(int i=0; i<test; i++){
			int n = sc.nextInt();
			for(int j=0; j<n; j++){
				int y = sc.nextInt();
				l1.addNode(y);
			}
			head = l1.reverse(head);
			l1.displayList(head);
		}
	}
}