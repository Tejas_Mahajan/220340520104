Create table DEPT(
DEPTNO int,
DNAME varchar(15),
LOC varchar(10));

Insert into DEPT values
(10, 'ACCOUNTING', 'NEW YORK'),
(20, 'RESEARCH', 'DALLAS'),
(30, 'SALES', 'CHICAGO'),
(40, 'OPERATIONS', 'BOSTON');

Create table EMP(
EMPNO int,
ENAME varchar(10),
JOB varchar(9),
HIREDATE date,
SAL float,
COMM float,
DEPTNO int);

Insert into EMP values
(7839, 'KING', 'MANAGER', '1991-11-17', 5000, NULL, 10),
(7698, 'BLAKE', 'CLERK', '1981-05-01', 2850, NULL, 30),
(7782, 'CLARK', 'MANAGER', '1981-06-09', 2450, NULL, 10),
(7566, 'JONES', 'CLERK', '1981-04-02', 2975, NULL, 20),
(7654, 'MARTIN', 'SALESMAN', '1981-09-28', 1250, 1400, 30),
(7499, 'ALLEN', 'SALESMAN', '1981-02-20', 1600, 300, 30);

-- Write SELECT statements to achieve the following:-
-- 3. Display all the employees where SAL between 2500 and 5000 (inclusive of both).
select ENAME from EMP where SAL between 2500 and 5000;

-- 4. Display all the ENAMEs in descending order of ENAME.
select ENAME from EMP order by ENAME desc;
 
-- 5. Display all the JOBs in lowercase.
select lower(JOB) "JOB" from EMP;

-- 6. Display the ENAMEs and the lengths of the ENAMEs.
select ENAME,length(ENAME) "LENGTH" from EMP;

-- 7. Display the DEPTNO and the count of employees who belong to that DEPTNO .
select DEPTNO,count(*) from EMP group by DEPTNO;

-- 8. Display the DNAMEs and the ENAMEs who belong to that DNAME.
select DNAME,ENAME from EMP,DEPT where DEPT.DEPTNO=EMP.DEPTNO;

-- 9. Display the position at which the string ‘AR’ occurs in the ename.
select ENAME from EMP where ENAME like '%AR%';

-- 10. Display the HRA for each employee given that HRA is 20% of SAL.
select ENAME,SAL,SAL*0.20 "HRA" from EMP;

-- Section II (10 marks)
/*1. Write a stored procedure by the name of PROC1 that accepts two varchar strings
as parameters. Your procedure should then determine if the first varchar string 
exists inside the varchar string. For example, if string1 = ‘DAC’ and string2 = 
‘CDAC, then string1 exists inside string2. The stored procedure should insert the 
appropriate message into a suitable TEMPP output table. Calling program for the 
stored procedure need not be written.*/
create table TEMPP(
STRING1 varchar(50),
STRING2 varchar(50));
alter table TEMPP add MESSAGE varchar(100);

delimiter //
create procedure PROC1(STRING1 varchar(50), STRING2 varchar(50))
begin
	declare message varchar(100);
    declare num int;
    select locate(STRING1,STRING2) into num;
    if num > 0 then 
		insert into TEMPP values(STRING1,STRING2,'STRING1 EXISTS IN STRING2');
    else
		insert into TEMPP values(STRING1,STRING2,'STRING1 does not exists in STRING2');
    end if;
    select * from TEMPP;
end; //
delimiter ;
call PROC1('DAC','CDAC');    

/*2. Create a stored function by the name of FUNC1 to take three parameters, the 
sides of a triangle. The function should return a Boolean value:- TRUE if the 
triangle is valid, FALSE otherwise. A triangle is valid if the length of each side is 
less than the sum of the lengths of the other two sides. Check if the dimensions 
entered can form a valid triangle. Calling program for the stored function need not 
be written.*/
create table TRIANGLE(
SIDE1 int,
SIDE2 int,
SIDE3 int,
REMARK varchar(50));

delimiter //
create procedure pqr(p int,q int,r int)
begin
	if FUNC1(p,q,r) then
		insert into TRIANGLE values (p,q,r,'VALID TRIANGLE');
    else
		insert into TRIANGLE values (p,q,r,'INVALID TRIANGLE');
    end if;
end; // 
delimiter ;
delimiter //
create function FUNC1(SIDE1 int,SIDE2 int,SIDE3 int)
returns boolean
deterministic
begin
		if SIDE1<(SIDE2+SIDE3) and SIDE3<(SIDE1+SIDE2) then
			return true;
        else
			return false;
        end if;
end; // 
delimiter ;

call pqr(10,20,30);
call pqr(20,40,50);
select * from TRIANGLE;        




