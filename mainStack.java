import java.util.*;

class stacksTwo{
	int[] arr;
	int size;
	int t1;
	int t2;
	
	stacksTwo(int n){
		size = n;
		arr = new int[n];
		t1 = -1;
		t2 = size;
	}
	
	void push1(int x){
		if(t1<t2-1){
			t1++;
			arr[t1]=x;
		}
		else{
			System.out.print("Stack Overflow");
			return;
		}
	}
	
	void push2(int x){
		if(t1<t2-1){
			t2--;
			arr[t2]=x;
		}
		else{
			System.out.println("Stack Overflow");
			return;
		}
	}
	
	int pop1(){
		if(t1 >= 0){
			int x = arr[t1];
			t1--;
			return x;
		}
		else{
			System.out.println("Stack Underflow");
			System.exit(1);
		}
		return 0;
	}
	
	int pop2(){
		if(t2<size){
			int x = arr[t2];
			t2++;
			return x;
		}
		else{
			System.out.print("Stack Underflow");
			System.exit(1);
		}
		return 0;
	}
}

class mainStack{
	public static void main(String[] args){
		stacksTwo s2 = new stacksTwo(6);
		s2.push1(5);
		s2.push2(10);
		s2.push2(15);
		s2.push1(11);
		s2.push2(7);
		s2.push2(40);
		
		System.out.println("Popped element from stack1 is "+s2.pop1());
		
		System.out.println("Popped element from stack2 is "+s2.pop2());
	}
}