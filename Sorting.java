import java.util.*;
class Sorting{
	static void insertionsort(int n,int a1[]){
		
		for(int i=1; i<n; i++){
			int k = a1[i];
			int j = i-1;
			while(j>=0 && a1[j]>k){
				a1[j+1] = a1[j];
				j = j-1;
				display(a1);
			}
			a1[j+1] = k;
		}
	}
	
	static void display(int a1[]){
		int n = a1.length;
		for(int i=0; i<n; i++){
			System.out.print(a1[i]+" ");
		}
		System.out.println("");
	}
	
	public static void main(String[] args){
		
		Scanner sc =new Scanner(System.in);
		int n = sc.nextInt();
		int a1[] = new int[n];
		for(int i=0; i<n; i++){
			a1[i]=sc.nextInt();
		}
		insertionsort(n,a1);
		display(a1);
		
	}

}